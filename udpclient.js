var dgram = require('dgram');
// create a server
var server = dgram.createSocket("udp4");
server.bind();
server.setBroadcast(true)
server.setMulticastTTL(128);
server.addMembership('230.185.192.108');

var serialport = require('node-serialport')

// get data in through the arduino serial port
var sp = new serialport.SerialPort("/dev/ttyACM0", {
     parser: serialport.parsers.readline('\n'),
})

// When there is data, send it out over udp
sp.on('data', function(chunk) {
    console.log(chunk.toString())
    var message = new Buffer(chunk.toString());
    server.send(message, 0, message.length, 33333, "230.185.192.108");
})

